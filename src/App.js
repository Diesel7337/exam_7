import './App.css';
import eat from './assets/eat.png';
import drink from './assets/coffee.png'
import Fastfood from "./components/Fastfood";

function App() {
  return (
    <div className="App">
      <Fastfood/>
    </div>
  );
}

export default App;
