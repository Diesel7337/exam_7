import {Component} from "react";
import './FastfoodStyle.css';
import eat from "../assets/eat.png";
import drink from "../assets/coffee.png";


export default class Fastfood extends Component {
    state = {
        Hamburger: 0,
        Cheeseburger: 0,
        Fries: 0,
        Coffee: 0,
        Tea: 0,
        Cola: 0
    }

    addRemoveItem = (action, item) => {
        let {
            Hamburger,
            Cheeseburger,
            Fries,
            Coffee,
            Tea,
            Cola
        } = this.state;

        let stateValue;
        switch (item){
            case 'Hamburger':{
                stateValue = Hamburger;
                break;
            }
            case 'Cheeseburger':{
                stateValue = Cheeseburger;
                break;
            }
            case 'Fries':{
                stateValue = Fries;
                break;
            }case 'Coffee':{
                stateValue = Coffee;
                break;
            }case 'Tea':{
                stateValue = Tea;
                break;
            }case 'Cola':{
                stateValue = Cola;
                break;
            }
            default: break;
        }

        if (action === 'add'){
            stateValue = stateValue +1;
        } else {
            stateValue = stateValue -1;
        }
        this.setState({
            [item]: stateValue >= 0 ? stateValue: 0
        });
    }
    itemContent = () => {
        let {
            Hamburger,
            Cheeseburger,
            Fries,
            Coffee,
            Tea,
            Cola
        } = this.state;
        let item = [];

        this.totalPrice = Hamburger * 80 + Cheeseburger * 90 + Fries * 45 + Coffee * 70 + Tea * 50 + Cola * 40;

        item.push(<p className="itemPrice">Hamburger {Hamburger} x 80 KGS</p>);
        item.push(<button className="down" onClick={() => this.addRemoveItem('remove','Hamburger')}>Down</button>)
        item.push(<p className="itemPrice">Cheeseburger {Cheeseburger} x 90 KGS</p>);
        item.push(<button className="down" onClick={() => this.addRemoveItem('remove','Cheeseburger')}>Down</button>)
        item.push(<p className="itemPrice">Fries {Fries} x 45 KGS</p>);
        item.push(<button className="down" onClick={() => this.addRemoveItem('remove','Fries')}>Down</button>)
        item.push(<p className="itemPrice">Coffee {Coffee} x 70 KGS</p>);
        item.push(<button className="down" onClick={() => this.addRemoveItem('remove','Coffee')}>Down</button>)
        item.push(<p className="itemPrice">Tea {Tea} x 50 KGS</p>);
        item.push(<button className="down" onClick={() => this.addRemoveItem('remove','Tea')}>Down</button>)
        item.push(<p className="itemPrice">Cola {Cola} x 40 KGS</p>);
        item.push(<button className="down" onClick={() => this.addRemoveItem('remove','Cola')}>Down</button>)

        return item;
        }

        render() {
            const { itemDiv } = this.state;
            return (
                    <div className="App">
                        <div className="Head">
                            <div className="Order-Details">
                                <h4>Order Details:</h4>
                                <p>Order is empty!</p>
                                <p>Please add some items!</p>
                                <div className="items">
                                    {this.itemContent()}
                                    <p>Total price: {this.totalPrice}</p>
                                </div>
                            </div>
                            <div className="Add-Items">
                                <h3>Add items:</h3>
                                <div className="Hamburger menu">
                                    <img src={eat} alt="" onClick={() => this.addRemoveItem('add', 'Hamburger')}/>
                                    <h1>Hamburger</h1>
                                    <p>Price 80 KGS</p>
                                </div>
                                <div className="Cheeseburger menu">
                                    <img src={eat} alt="" onClick={() => this.addRemoveItem('add', 'Cheeseburger')}/>
                                    <h1>Cheeseburger</h1>
                                    <p>Price 90 KGS</p>
                                </div>
                                <div className="Fries menu">
                                    <img src={eat} alt="" onClick={() => this.addRemoveItem('add', 'Fries')}/>
                                    <h1>Fries</h1>
                                    <p>Price 45 KGS</p>
                                </div>
                                <div className="Coffee menu">
                                    <img src={drink} alt="" onClick={() => this.addRemoveItem('add', 'Coffee')}/>
                                    <h1>Coffee</h1>
                                    <p>Price 70 KGS</p>
                                </div>
                                <div className="Tea menu">
                                    <img src={drink} alt="" onClick={() => this.addRemoveItem('add', 'Tea')}/>
                                    <h1>Tea</h1>
                                    <p>Price 50 KGS</p>
                                </div>
                                <div className="Cola menu">
                                    <img src={drink} alt="" onClick={() => this.addRemoveItem('add', 'Cola')}/>
                                    <h1>Cola</h1>
                                    <p>Price 40 KGS</p>
                                </div>
                            </div>
                        </div>
                    </div>
            );
        }
}